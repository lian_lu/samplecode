package dbinterface;

/**
 * @author lianlu 
 */
/**
 * data describes the serializable data storing table header
 */
class data {
	String name;

	String type;
	/**
	 * whether the current column is a primary key
	 */
	boolean isPrimaryKey;

	public data(String name, String type, boolean is) {
		this.name = name;
		this.type = type;
		this.isPrimaryKey = is;

	}
}
