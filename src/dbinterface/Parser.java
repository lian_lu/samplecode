package dbinterface;

import java.util.HashSet;
/** 
 * Co-author: Brian Vicent
 * 			  Lian Lu		         
 */

/**
 * Parser will work as a tool class. To generate a parser, the input will be the
 * SQL command, the output will be a command accordingly. For example,
 * <p>
 * <div>String test1 =
 * "CREATE TABLE cola_markets (mkt_id NUMBER PRIMARY KEY, name VARCHAR2(32), shape SDO_GEOMETRY)"
 * ; </div> <div>Parser parser = new Parser(); </div> <div> Command cm =
 * parser.createCommand(test1); </div>
 * </p>
 */

public class Parser {

	private static HashSet<String> comset = null;

	/**
	 * Initialized the hashset comset, storing the valid SQL command prefix.
	 */
	public Parser() {
		comset = new HashSet<String>();
		comset.add("CREATE INDEX");
		comset.add("CREATE TABLE");
		comset.add("INSERT");
		comset.add("SELECT");
	}

	/**
	 * 
	 * @param SQL
	 *            command, in String format
	 * @return Command object that can Start()
	 * @throws Exception
	 *             Invalid command
	 */
	public static Command createCommand(String cmd) throws Exception {

		String[] cm = cmd.split("\\s+"); // split the input by spaces
		String s1 = cm[0];
		String s2 = (cm[0] + " " + cm[1]).toUpperCase();
		if (!(comset.contains(s1) || comset.contains(s2))) { // the command is
																// not in the
																// syntax set
			System.out.println("Wrong Commd");
			return null;
		}
		if (s1.equals("CREATE")) {
			if (s2.equals("Create Table".toUpperCase()))
				return new CreateCommand(cmd);
			else if (s2.equals("Create Index".toUpperCase()))
				return new IndexCommand(cmd);
			else {
				throw new Exception("Invalid Syntax");
			}

		} else if (s2.equals("Insert into".toUpperCase()))
			return new InsertCommand(cmd);
		else if (s1.equals("SELECT"))
			return new SelectionCommand(cmd);
		else {
			throw new Exception("Invalid Syntax");
		}
	}
}
