package dbinterface;

/**
 * @author lian lu 
 */

/**
 * Abstract class represents parent command. All commands, including
 * CreateCommand.java, insert.java and select.java must extends it. Each command
 * starts to conduct operation by calling Start().
 */
public abstract class Command {
	/**
	 * complete SQL command
	 */
	protected String _cmd;

	/**
	 * command to start execution of command
	 */
	abstract void Start();

}
