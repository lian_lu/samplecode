package dbinterface;

import heap.Heapfile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 *  Author: Lian Lu
*/
/**
 * A CreateCommand parer is generated. The create command information is
 * collected.
 * <p>
 * The input is follows: <div>CREATE TABLE cola_markets (mkt_id NUMBER PRIMARY
 * KEY, name VARCHAR2(32), shape SDO_GEOMETRY); </div> The output will be a
 * CreateCommand class object, that can call Start() to create a table.
 * </p>
 */

public class CreateCommand extends Command {

	private List<data> header; // the header of a table, e.g., mkt_id, name,
								// shape
	private String tablename; // table name

	//
	/**
	 * Constructor
	 * 
	 * @param SQL
	 *            command in String
	 */
	public CreateCommand(String comm) {
		this._cmd = comm;
		String[] com = comm.split("\\s+");
		if (!(com[0] + com[1]).toUpperCase().equals("CREATETABLE") || com.length <= 3) {
			throw new Exception("Invalid Syntax");
		}

		this.tablename = com[2]; // store the table name

		int startindex = 0;
		while (comm.charAt(startindex) != '(')
			startindex++;
		startindex++;
		String attr = comm.substring(startindex);
		String[] attrs = attr.split(",");
		header = new ArrayList<data>();
		for (String a : attrs) {
			a = a.trim();
			String[] temp = a.split(" ");
			if (temp.length == 2) {
				header.add(new data(temp[0], temp[1], false));
			} else
				header.add(new data(temp[0], temp[1], true));

		}

	}

	/**
	 * Start to run create command
	 */
	public void Start() {

		Heapfile f = null;

		f = CreateTable(this.tablename, this.header);

	}

	private Heapfile CreateTable(String tablename, List<data> list) {
		tableinfo.put(tablename, list);
		Heapfile f = null;
		try {
			f = new Heapfile(tablename);
		} catch (HFException | HFBufMgrException | HFDiskMgrException | IOException e) {
			e.printStackTrace();
		}
		return f;
	}

}
